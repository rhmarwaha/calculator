//
//  ViewController.swift
//  Calculator
//
//  Created by deltaschool on 09/09/19.
//  Copyright © 2019 deltaschool. All rights reserved.
//

import UIKit

enum Tags: Int{
    case zero = 1
    case one
    case two
    case three
    case four
    case five
    case six
    case seven
    case eight
    case nine
    case dot
    case equal
    case minus
    case multiply
    case plus
    case divide
    case delete
    case plusminus
    case ac
}


class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var result: UILabel!
    var operations: String?
    var resulter: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    @IBAction func number(_ sender: UIButton) {
        
        switch sender.tag {
        case Tags.zero.rawValue:
            print("0")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "0"
                }else{
                    result.text = value + "0"
                    
                }
            }
            break
        case Tags.one.rawValue:
            print("1")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "1"
                }else{
                                        result.text = value + "1"
                    
                }
            }
            break
            
        case Tags.two.rawValue:
            print("2")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "2"
                }else{
                    
                        result.text = value + "2"
                    
                }
            }
            break
            
        case Tags.three.rawValue:
            print("3")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "3"
                }else{
                    
                        result.text = value + "3"
                    
                }
            }
            break
            
        case Tags.four.rawValue:
            print("4")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "4"
                }else{
                   result.text = value + "4"
                }
            }
            break
            
        case Tags.five.rawValue:
            print("5")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "5"
                }else{
                   
                        result.text = value + "5"
                    
                }
            }
            break
            
        case Tags.six.rawValue:
            print("6")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "6"
                }else{
                        result.text = value + "6"
                    
                }
            }
            break
            
            
        case Tags.seven.rawValue:
            print("7")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "7"
                }else{
                        result.text = value + "7"
                    
                }
            }
            break
            
        case Tags.eight.rawValue:
            print("8")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "8"
                }else{
                        result.text = value + "8"
                    
                }
            }
            break
        case Tags.nine.rawValue:
            print("9")
            if let value = result.text {
                if Int(value) == 0 {
                    result.text = "9"
                }else{
                        result.text = value + "9"
                    
                }
            }
            break
            
        case Tags.dot.rawValue:
            print(".")
            if let value = result.text {
                let separators = CharacterSet(charactersIn: "+,-,/,*")
                var parts =  value.components(separatedBy: separators)
                print(parts)
                let lastValue = parts[parts.count - 1]
                if !lastValue.contains("."){
                    result.text = value+"."
                    operations = value+"."
                }
                
            }
            break
            
        default:
            print("nothing else")
        }

    }
    
    @IBAction func operations(_ sender: UIButton) {
        
        switch sender.tag {
            
        case Tags.equal.rawValue :
            print("=")
            if let res = result.text  {
                let str = String(res.last!)
                if !(str.contains("+") || str.contains("-") || str.contains("/") || str.contains("*")) {
                    let resultValue = NSExpression(format: res)
                    let resultNumber = resultValue.expressionValue(with: nil, context: nil) as? Double
                    result.text = String(resultNumber!)
                    
                }
                }
            
            
        case Tags.minus.rawValue :
            if let value = result.text {
                let str = String(value.last!)
                if !(str.contains("+") || str.contains("-") || str.contains("/") || str.contains("*")) {
                    operations = value + "-"
                    result.text = operations
                    
                }
                
                
            }
            
            
        case Tags.multiply.rawValue :
            print("*")
            if let value = result.text {
                 let str = String(value.last!)
                if !(str.contains("+") || str.contains("-") || str.contains("/") || str.contains("*")) {
                    operations = value + "*"
                    result.text = operations
                    
                }
                
                }
            
        case Tags.plus.rawValue :
            print("+")
            if let value = result.text {
                
                let str = String(value.last!)
                if !(str.contains("+") || str.contains("-") || str.contains("/") || str.contains("*")) {
                    operations = value + "+"
                    result.text = operations
                    
                }
                
            }
            
        case Tags.divide.rawValue :
            print("/")
            if let value = result.text {
                
                let str = String(value.last!)
                if !(str.contains("+") || str.contains("-") || str.contains("/") || str.contains("*")) {
                  
                    operations = value + "/"
                    result.text = operations
                }
            }
            
        case Tags.delete.rawValue :
            print("DEL")
            if let value = result.text {
                //let removing:String = value.remove(at: value.index(after: value.endIndex))
                result.text = String(value.dropLast())
                operations = String(value.dropLast())
                
            }
            
        case Tags.plusminus.rawValue :
            print("+/-")
            if let val = result.text {
                if let value = Double(val){
                      if value > 0.0 {
                      result.text = "-" + val
                      }else{
                        result.text = String(abs(value))
                      }
                }
            }
            
        case Tags.ac.rawValue :
            print("C")
         
            operations = nil
            result.text = "0"
            
            
        default: break
            
        }
    }
    
}

